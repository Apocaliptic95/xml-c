﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XML.DataModels
{
    public abstract class Item
    {
        public string Id
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Occurrence
        {
            get; set;
        }
        public string ItemPrice
        {
            get; set;
        }
        public string AllPrice
        {
            get
            {
                int price,sum=0;
                if(int.TryParse(ItemPrice,out price))
                {
                    sum += price;
                }
                foreach(Property prop in Properties)
                {
                    if (int.TryParse(prop.Price, out price))
                    {
                        sum += price;
                    }
                }
                return sum.ToString();
            }
        }
        public string Property
        {
            get; set;
        }
        public string Property2
        {
            get; set;
        }
        public string maxAssignments
        {
            get; set;
        }
        public string Path
        {
            get; set;
        }
        public List<Property> Properties;
        public ItemType Type
        {
            get;
            protected set;
        }

        public Item()
        {
            Properties = new List<Property>();
        }
    }

    public class Property
    {
        public string Id
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Price
        {
            get; set;
        }
        public string Description
        {
            get; set;
        }
        public PropertyUnit Unit
        {
            get; set;
        }
        public string Value
        {
            get; set;
        }
        public string FullValue
        {
            get
            {
                string unit = "";
                if (Unit == PropertyUnit.percentage)
                    unit = "%";
                return Value+unit;
            }
        }
    }

    public class Weapon : Item
    {
        public string MinDamage
        {
            get
            {
                return Property;
            }
            set
            {
                Property = value;
            }
        }
        public string MaxDamage
        {
            get
            {
                return Property2;
            }
            set
            {
                Property2 = value;
            }
        }
        public Weapon()
        {
            Type = ItemType.Weapon;
        }
    }

    public class Armour : Item
    {
        public string Defense
        {
            get
            {
                return Property;
            }
            set
            {
                Property = value;
            }
        }
        public Armour()
        {
            Type = ItemType.Armour;
        }
    }

    public class Helm : Item
    {
        public string Defense
        {
            get; set;
        }
        public Helm()
        {
            Type = ItemType.Helm;
        }
    }

    public class Gloves: Item
    {
        public string Defense
        {
            get; set;
        }
        public Gloves()
        {
            Type = ItemType.Gloves;
        }
    }

    public class Belt : Item
    {
        public string Defense
        {
            get; set;
        }
        public Belt()
        {
            Type = ItemType.Belt;
        }
    }

    public class Ring : Item
    {
        public Ring()
        {
            Type = ItemType.Ring;
        }
    }

    public class Amulet : Item
    {
        public Amulet()
        {
            Type = ItemType.Amulet;
        }
    }

    public class Mixture : Item
    {
        public Mixture()
        {
            Type = ItemType.Mixture;
        }
    }

    public class Other : Item
    {
        public Other()
        {
            Type = ItemType.Other;
        }
    }
}
