﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;
using XML.DataModels;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Controls;
using System.Windows;
using System.Dynamic;

namespace XML
{
    public class MainWindowViewModel : Screen
    {
        XMLRPG xmlProvider = new XMLRPG();

        public MainWindowViewModel()
        {
            NameColor = "#FF000000";
            if (xmlProvider.Load("items.xml"))
            {
                _weapons = new BindableCollection<Weapon>(xmlProvider.getWeapons());
                _armours = new BindableCollection<Armour>(xmlProvider.getArmours());
                _helms = new BindableCollection<Helm>(xmlProvider.getHelms());
                _gloves = new BindableCollection<Gloves>(xmlProvider.getGloves());
                _belts = new BindableCollection<Belt>(xmlProvider.getBelts());
                _rings = new BindableCollection<Ring>(xmlProvider.getRings());
                _amulets = new BindableCollection<Amulet>(xmlProvider.getAmulets());
                _mixtures = new BindableCollection<Mixture>(xmlProvider.getMixtures());
                _others = new BindableCollection<Other>(xmlProvider.getOthers());
            }
            else
            {
                MessageBox.Show("Xml file failed to load.", "Error");
                this.DeactivateWith(this);
                _weapons = new BindableCollection<Weapon>();
                _armours = new BindableCollection<Armour>();
                _helms = new BindableCollection<Helm>();
                _gloves = new BindableCollection<Gloves>();
                _belts = new BindableCollection<Belt>();
                _rings = new BindableCollection<Ring>();
                _amulets = new BindableCollection<Amulet>();
                _mixtures = new BindableCollection<Mixture>();
                _others = new BindableCollection<Other>();
            }
            WeaponIndex = -1;
            ArmourIndex = -1;
            HelmIndex = -1;
            GlovesIndex = -1;
            BeltIndex = -1;
            RingIndex = -1;
            AmuletIndex = -1;
            MixtureIndex = -1;
            OtherIndex = -1;
            MixtureIndex = -1;
        }

        #region ControlData

        private Result _resultDialog;
        public Result ResultDialog
        {
            get
            {
                return _resultDialog;
            }
            set
            {
                _resultDialog = value;
                NotifyOfPropertyChange("ResultDialog");
                MessageBox.Show(ResultDialog.ToString());
            }
        }

        private Item _resultItem;
        public Item ResultItem
        {
            get
            {
                return _resultItem;
            }
            set
            {
                _resultItem = value;
                NotifyOfPropertyChange("ResultItem");
            }
        }

        private Property _resultProperty;
        public Property ResultProperty
        {
            get
            {
                return _resultProperty;
            }
            set
            {
                _resultProperty = value;
                NotifyOfPropertyChange("ResultProperty");
            }
        }

        private Item _lastSelected;
        public Item LastSelected
        {
            get
            {
                return _lastSelected;
            }
            set
            {
                _lastSelected = value;
                NotifyOfPropertyChange("LastSelected");
                NotifyOfPropertyChange(() => CanPropertyAdd);
                NotifyOfPropertyChange(() => CanPropertyList);
                NotifyOfPropertyChange(() => CanItemEdit);
                NotifyOfPropertyChange(() => CanItemRemove);
                ItemChange((LastSelected != null) ? LastSelected.Type : ItemType.Weapon);
            }
        }

        private int _weaponIndex;
        public int WeaponIndex
        {
            get
            {
                return _weaponIndex;
            }
            set
            {
                _weaponIndex = value;
                NotifyOfPropertyChange("WeaponIndex");
            }
        }

        private int _armourIndex;
        public int ArmourIndex
        {
            get
            {
                return _armourIndex;
            }
            set
            {
                _armourIndex = value;
                NotifyOfPropertyChange("ArmourIndex");
            }
        }

        private int _helmIndex;
        public int HelmIndex
        {
            get
            {
                return _helmIndex;
            }
            set
            {
                _helmIndex = value;
                NotifyOfPropertyChange("HelmIndex");
            }
        }

        private int _glovesIndex;
        public int GlovesIndex
        {
            get
            {
                return _glovesIndex;
            }
            set
            {
                _glovesIndex = value;
                NotifyOfPropertyChange("GlovesIndex");
            }
        }

        private int _beltIndex;
        public int BeltIndex
        {
            get
            {
                return _beltIndex;
            }
            set
            {
                _beltIndex = value;
                NotifyOfPropertyChange("BeltIndex");
            }
        }

        private int _ringIndex;
        public int RingIndex
        {
            get
            {
                return _ringIndex;
            }
            set
            {
                _ringIndex = value;
                NotifyOfPropertyChange("RingIndex");
            }
        }

        private int _amuletIndex;
        public int AmuletIndex
        {
            get
            {
                return _amuletIndex;
            }
            set
            {
                _amuletIndex = value;
                NotifyOfPropertyChange("AmuletIndex");
            }
        }

        private int _mixtureIndex;
        public int MixtureIndex
        {
            get
            {
                return _mixtureIndex;
            }
            set
            {
                _mixtureIndex = value;
                NotifyOfPropertyChange("MixtureIndex");
            }
        }

        private int _otherIndex;
        public int OtherIndex
        {
            get
            {
                return _otherIndex;
            }
            set
            {
                _otherIndex = value;
                NotifyOfPropertyChange("OtherIndex");
            }
        }

        private ImageSource _image;
        public ImageSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                NotifyOfPropertyChange("Image");
            }
        }

        private string _itemName;
        public string ItemName
        {
            get
            {
                return _itemName;
            }
            set
            {
                _itemName = value;
                NotifyOfPropertyChange("ItemName");
            }
        }

        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
            set
            {
                _propertyName = value;
                NotifyOfPropertyChange("PropertyName");
            }
        }

        private string _propertyContent;
        public string PropertyContent
        {
            get
            {
                return _propertyContent;
            }
            set
            {
                _propertyContent = value;
                NotifyOfPropertyChange("PropertyContent");
            }
        }

        private string _priceContent;
        public string PriceContent
        {
            get
            {
                return _priceContent;
            }
            set
            {
                _priceContent = "Price: "+value+" Gold";
                NotifyOfPropertyChange("PriceContent");
            }
        }

        private string _nameColor;
        public string NameColor
        {
            get
            {
                return _nameColor;
            }
            set
            {
                _nameColor = value;
                NotifyOfPropertyChange("NameColor");
            }
        }

        private BindableCollection<Weapon> _weapons;
        public BindableCollection<Weapon> Weapons
        {
            get
            {
                return _weapons;
            }
            set
            {
                _weapons = value;
                NotifyOfPropertyChange("Weapons");
            }
        }
        private Weapon _selectedWeapon;
        public Weapon SelectedWeapon
        {
            get
            {
                return _selectedWeapon;
            }
            set
            {
                _selectedWeapon = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedWeapon");
                ItemChange(ItemType.Weapon);
            }
        }

        private BindableCollection<Armour> _armours;
        public BindableCollection<Armour> Armours
        {
            get
            {
                return _armours;
            }
            set
            {
                _armours = value;
                NotifyOfPropertyChange("Armours");
            }
        }
        private Armour _selectedarmour;
        public Armour SelectedArmour
        {
            get
            {
                return _selectedarmour;
            }
            set
            {
                _selectedarmour = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedArmour");
                ItemChange(ItemType.Armour);
            }
        }

        private BindableCollection<Helm> _helms;
        public BindableCollection<Helm> Helms
        {
            get
            {
                return _helms;
            }
            set
            {
                _helms = value;
                NotifyOfPropertyChange("Helms");
            }
        }
        private Helm _selectedHelm;
        public Helm SelectedHelm
        {
            get
            {
                return _selectedHelm;
            }
            set
            {
                _selectedHelm = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedHelm");
                ItemChange(ItemType.Helm);
            }
        }

        private BindableCollection<Gloves> _gloves;
        public BindableCollection<Gloves> Gloves
        {
            get
            {
                return _gloves;
            }
            set
            {
                _gloves = value;
                NotifyOfPropertyChange("Gloves");
            }
        }
        private Gloves _selectedGlove;
        public Gloves SelectedGlove
        {
            get
            {
                return _selectedGlove;
            }
            set
            {
                _selectedGlove = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedGlove");
                ItemChange(ItemType.Gloves);
            }
        }

        private BindableCollection<Belt> _belts;
        public BindableCollection<Belt> Belts
        {
            get
            {
                return _belts;
            }
            set
            {
                _belts = value;
                NotifyOfPropertyChange("Belts");
            }
        }
        private Belt _selectedBelt;
        public Belt SelectedBelt
        {
            get
            {
                return _selectedBelt;
            }
            set
            {
                _selectedBelt = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedBelt");
                ItemChange(ItemType.Belt);
            }
        }

        private BindableCollection<Ring> _rings;
        public BindableCollection<Ring> Rings
        {
            get
            {
                return _rings;
            }
            set
            {
                _rings = value;
                NotifyOfPropertyChange("Rings");
            }
        }
        private Ring _selectedRing;
        public Ring SelectedRing
        {
            get
            {
                return _selectedRing;
            }
            set
            {
                _selectedRing = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedRing");
                ItemChange(ItemType.Ring);
            }
        }

        private BindableCollection<Amulet> _amulets;
        public BindableCollection<Amulet> Amulets
        {
            get
            {
                return _amulets;
            }
            set
            {
                _amulets = value;
                NotifyOfPropertyChange("Amulets");
            }
        }
        private Amulet _selectedAmulet;
        public Amulet SelectedAmulet
        {
            get
            {
                return _selectedAmulet;
            }
            set
            {
                _selectedAmulet = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedAmulet");
                ItemChange(ItemType.Amulet);
            }
        }

        private BindableCollection<Mixture> _mixtures;
        public BindableCollection<Mixture> Mixtures
        {
            get
            {
                return _mixtures;
            }
            set
            {
                _mixtures = value;
                NotifyOfPropertyChange("Mixtures");
            }
        }
        private Mixture _selectedMixture;
        public Mixture SelectedMixture
        {
            get
            {
                return _selectedMixture;
            }
            set
            {
                _selectedMixture = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedMixture");
                ItemChange(ItemType.Mixture);
            }
        }

        private BindableCollection<Other> _others;
        public BindableCollection<Other> Others
        {
            get
            {
                return _others;
            }
            set
            {
                _others = value;
                NotifyOfPropertyChange("Others");
            }
        }
        private Other _selectedOther;
        public Other SelectedOther
        {
            get
            {
                return _selectedOther;
            }
            set
            {
                _selectedOther = value;
                LastSelected = value;
                NotifyOfPropertyChange("SelectedOther");
                ItemChange(ItemType.Other);
            }
        }

        private BindableCollection<Property> _properties;
        public BindableCollection<Property> Properties
        {
            get
            {
                return _properties;
            }
            set
            {
                _properties = value;
                NotifyOfPropertyChange("Properties");
            }
        }
        private Property _selectedProperty;
        public Property SelectedProperty
        {
            get
            {
                return _selectedProperty;
            }
            set
            {
                _selectedProperty = value;
                NotifyOfPropertyChange("SelectedProperty");
                NotifyOfPropertyChange(() => CanPropertyEdit);
                NotifyOfPropertyChange(() => CanPropertyRemove);
            }
        }

        #endregion

        private void ItemChange(ItemType itemType)
        {
            if(LastSelected != null)
            switch(itemType)
            {
                case ItemType.Weapon:
                        Uri path = new Uri(SelectedWeapon.Path.Substring(2, SelectedWeapon.Path.Length - 2), UriKind.Relative);
                        BitmapImage src = new BitmapImage();
                        src.BeginInit();
                        src.UriSource = path;
                        src.CacheOption = BitmapCacheOption.OnLoad;
                        src.EndInit();
                        Image = src;
                    if (int.Parse(SelectedWeapon.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedWeapon.Occurrence) >= 3 && int.Parse(SelectedWeapon.Occurrence) <=7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedWeapon.Name;
                    PropertyName = "Damage:";
                    PropertyContent = SelectedWeapon.MinDamage+"-"+SelectedWeapon.MaxDamage;
                    PriceContent = SelectedWeapon.AllPrice;
                    Properties = new BindableCollection<Property>(SelectedWeapon.Properties);
                    break;

                case ItemType.Armour:
                        Uri path2 = new Uri(SelectedArmour.Path.Substring(2, SelectedArmour.Path.Length - 2), UriKind.Relative);
                        BitmapImage src2 = new BitmapImage();
                        src2.BeginInit();
                        src2.UriSource = path2;
                        src2.CacheOption = BitmapCacheOption.OnLoad;
                        src2.EndInit();
                        Image = src2;
                        if (int.Parse(SelectedArmour.Occurrence) < 3)
                        NameColor = "#FF000000";
                        else if (int.Parse(SelectedArmour.Occurrence) >= 3 && int.Parse(SelectedArmour.Occurrence) <= 7)
                            NameColor = "#FF0000FF";
                        else
                            NameColor = "#FF00FF00";
                        ItemName = SelectedArmour.Name;
                        PropertyName = "Defense:";
                        PropertyContent = SelectedArmour.Defense;
                        PriceContent = SelectedArmour.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedArmour.Properties);
                        break;
                case ItemType.Helm:
                        Uri path3 = new Uri(SelectedHelm.Path.Substring(2, SelectedHelm.Path.Length - 2), UriKind.Relative);
                        BitmapImage src3 = new BitmapImage();
                        src3.BeginInit();
                        src3.UriSource = path3;
                        src3.CacheOption = BitmapCacheOption.OnLoad;
                        src3.EndInit();
                        Image = src3;
                        if (int.Parse(SelectedHelm.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedHelm.Occurrence) >= 3 && int.Parse(SelectedHelm.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedHelm.Name;
                    PropertyName = "Defense:";
                    PropertyContent = SelectedHelm.Defense;
                    PriceContent = SelectedHelm.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedHelm.Properties);
                        break;
                case ItemType.Gloves:
                        Uri path4 = new Uri(SelectedGlove.Path.Substring(2, SelectedGlove.Path.Length - 2), UriKind.Relative);
                        BitmapImage src4 = new BitmapImage();
                        src4.BeginInit();
                        src4.UriSource = path4;
                        src4.CacheOption = BitmapCacheOption.OnLoad;
                        src4.EndInit();
                        Image = src4;
                        if (int.Parse(SelectedGlove.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedGlove.Occurrence) >= 3 && int.Parse(SelectedGlove.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedGlove.Name;
                    PropertyName = "Defense:";
                    PropertyContent = SelectedGlove.Defense;
                    PriceContent = SelectedGlove.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedGlove.Properties);
                        break;
                case ItemType.Belt:
                        Uri path5 = new Uri(SelectedBelt.Path.Substring(2, SelectedBelt.Path.Length - 2), UriKind.Relative);
                        BitmapImage src5 = new BitmapImage();
                        src5.BeginInit();
                        src5.UriSource = path5;
                        src5.CacheOption = BitmapCacheOption.OnLoad;
                        src5.EndInit();
                        Image = src5;
                        if (int.Parse(SelectedBelt.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedBelt.Occurrence) >= 3 && int.Parse(SelectedBelt.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedBelt.Name;
                    PropertyName = "Defense:";
                    PropertyContent = SelectedBelt.Defense;
                    PriceContent = SelectedBelt.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedBelt.Properties);
                        break;
                case ItemType.Ring:
                        Uri path6 = new Uri(LastSelected.Path.Substring(2, LastSelected.Path.Length - 2), UriKind.Relative);
                        BitmapImage src6 = new BitmapImage();
                        src6.BeginInit();
                        src6.UriSource = path6;
                        src6.CacheOption = BitmapCacheOption.OnLoad;
                        src6.EndInit();
                        Image = src6;
                        if (int.Parse(SelectedRing.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedRing.Occurrence) >= 3 && int.Parse(SelectedRing.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedRing.Name;
                    PropertyName = "";
                    PropertyContent = "";
                    PriceContent = SelectedRing.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedRing.Properties);
                        break;
                case ItemType.Amulet:
                        Uri path7 = new Uri(SelectedAmulet.Path.Substring(2, SelectedAmulet.Path.Length - 2), UriKind.Relative);
                        BitmapImage src7 = new BitmapImage();
                        src7.BeginInit();
                        src7.UriSource = path7;
                        src7.CacheOption = BitmapCacheOption.OnLoad;
                        src7.EndInit();
                        Image = src7;
                        if (int.Parse(SelectedAmulet.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedAmulet.Occurrence) >= 3 && int.Parse(SelectedAmulet.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedAmulet.Name;
                    PropertyName = "";
                    PropertyContent = "";
                    PriceContent = SelectedAmulet.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedAmulet.Properties);
                        break;
                case ItemType.Mixture:
                        Uri path8 = new Uri(SelectedMixture.Path.Substring(2, SelectedMixture.Path.Length - 2), UriKind.Relative);
                        BitmapImage src8 = new BitmapImage();
                        src8.BeginInit();
                        src8.UriSource = path8;
                        src8.CacheOption = BitmapCacheOption.OnLoad;
                        src8.EndInit();
                        Image = src8;
                        if (int.Parse(SelectedMixture.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedMixture.Occurrence) >= 3 && int.Parse(SelectedMixture.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedMixture.Name;
                    PropertyName = "";
                    PropertyContent = "";
                    PriceContent = SelectedMixture.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedMixture.Properties);
                        break;
                case ItemType.Other:
                        Uri path9 = new Uri(SelectedOther.Path.Substring(2, SelectedOther.Path.Length - 2), UriKind.Relative);
                        BitmapImage src9 = new BitmapImage();
                        src9.BeginInit();
                        src9.UriSource = path9;
                        src9.CacheOption = BitmapCacheOption.OnLoad;
                        src9.EndInit();
                        Image = src9;
                        if (int.Parse(SelectedOther.Occurrence) < 3)
                        NameColor = "#FF000000";
                    else if (int.Parse(SelectedOther.Occurrence) >= 3 && int.Parse(SelectedOther.Occurrence) <= 7)
                        NameColor = "#FF0000FF";
                    else
                        NameColor = "#FF00FF00";
                    ItemName = SelectedOther.Name;
                    PropertyName = "";
                    PropertyContent = "";
                    PriceContent = SelectedOther.AllPrice;
                        Properties = new BindableCollection<Property>(SelectedOther.Properties);
                        break;
            }
        }

        public void TabsSelectionChanged(Object sender, SelectionChangedEventArgs args)
        {
            if(args.Source is TabControl)
            {
                WeaponIndex = -1;
                ArmourIndex = -1;
                HelmIndex = -1;
                GlovesIndex = -1;
                BeltIndex = -1;
                RingIndex = -1;
                AmuletIndex = -1;
                MixtureIndex = -1;
                OtherIndex = -1;
                Properties = new BindableCollection<Property>();
            }           
        }

        public bool CanPropertyAdd
        {
            get
            {
                if (LastSelected != null)
                    return true;
                else
                    return false;
            }
        }
        public void PropertyAdd()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.ResizeMode = ResizeMode.NoResize;
            settings.Width = 345.82;
            settings.Height = 255.828;
            settings.Title = "Add new Property";

            IWindowManager manager = new WindowManager();
            AddPropertyViewModel add = new AddPropertyViewModel(SelectedProperty, Action.Add);
            bool? result = manager.ShowDialog(add, null, settings);
            ResultProperty = add.ResultProperty;
            if (result == true)
            {
                LastSelected.Properties.Add(ResultProperty);

                xmlProvider.AddProperty(ResultProperty, LastSelected);
                Item temp = LastSelected;
                LastSelected = null;
                LastSelected = temp;
            }
        }

        public bool CanPropertyList
        {
            get
            {
                return CanPropertyAdd;
            }
        }
        public void PropertyList()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.ResizeMode = ResizeMode.NoResize;
            settings.Width = 300;
            settings.Height = 300;
            settings.Title = "Add new Property";

            IWindowManager manager = new WindowManager();
            PropertyListViewModel add = new PropertyListViewModel(xmlProvider.getProperties());
            bool? result = manager.ShowDialog(add, null, settings);
            ResultProperty = add.ResultProperty;
            if (result == true)
            {
                LastSelected.Properties.Add(ResultProperty);
                xmlProvider.AddProperty(ResultProperty, LastSelected);
                Item temp = LastSelected;
                LastSelected = null;
                LastSelected = temp;
            }
        }

        public bool CanPropertyEdit
        {
            get
            {
                if (SelectedProperty != null)
                    return true;
                else
                    return false;
            }
        }
        public void PropertyEdit()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.ResizeMode = ResizeMode.NoResize;
            settings.Width = 345.82;
            settings.Height = 255.828;
            settings.Title = "Edit Property";

            IWindowManager manager = new WindowManager();
            AddPropertyViewModel add = new AddPropertyViewModel(SelectedProperty, Action.Edit);
            bool? result = manager.ShowDialog(add, null, settings);
            ResultProperty = add.ResultProperty;
            if (result == true)
            {
                SelectedProperty.Name = ResultProperty.Name;
                SelectedProperty.Price = ResultProperty.Price;
                SelectedProperty.Unit = ResultProperty.Unit;
                SelectedProperty.Value = ResultProperty.Value;
                SelectedProperty.Description = ResultProperty.Description;
                NotifyOfPropertyChange(() => Properties);
                xmlProvider.EditProperty(SelectedProperty);
                Item temp = LastSelected;
                LastSelected = null;
                LastSelected = temp;
            }
        }

        public bool CanPropertyRemove
        {
            get
            {
                if (SelectedProperty != null)
                    return true;
                else
                    return false;
            }
        }
        public void PropertyRemove()
        {
            MessageBoxResult result = MessageBox.Show("Do you want remove this property?", "Removing", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                LastSelected.Properties.Remove(SelectedProperty);
                xmlProvider.RemoveProperty(SelectedProperty, LastSelected);
                Item temp = LastSelected;
                LastSelected = null;
                LastSelected = temp;
            }
        }

        public bool CanItemAdd
        {
            get
            {
                return true;
            }
        }
        public void ItemAdd()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.ResizeMode = ResizeMode.NoResize;
            settings.Width = 345.82;
            settings.Height = 255.828;
            settings.Title = "Add new Item";

            IWindowManager manager = new WindowManager();
            AddItemViewModel add = new AddItemViewModel(LastSelected, Action.Add);
            bool? result = manager.ShowDialog(add, null, settings);
            ResultItem = add.ResultItem;
            if(result == true)
            {
                switch (ResultItem.Type)
                {
                    case ItemType.Weapon:
                        Weapons.Add((Weapon)ResultItem);
                        break;
                    case ItemType.Armour:
                        Armours.Add((Armour)ResultItem);
                        break;
                    case ItemType.Helm:
                        Helms.Add((Helm)ResultItem);
                        break;
                    case ItemType.Gloves:
                        Gloves.Add((Gloves)ResultItem);
                        break;
                    case ItemType.Belt:
                        Belts.Add((Belt)ResultItem);
                        break;
                    case ItemType.Ring:
                        Rings.Add((Ring)ResultItem);
                        break;
                    case ItemType.Amulet:
                        Amulets.Add((Amulet)ResultItem);
                        break;
                    case ItemType.Mixture:
                        Mixtures.Add((Mixture)ResultItem);
                        break;
                    case ItemType.Other:
                        Others.Add((Other)ResultItem);
                        break;
                }
                xmlProvider.AddItem(ResultItem);
            }
            else
            {

            }
        }

        public bool CanItemEdit
        {
            get
            {
                if (LastSelected != null)
                    return true;
                else
                    return false;
            }
        }
        public void ItemEdit()
        {
            dynamic settings = new ExpandoObject();
            settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            settings.ResizeMode = ResizeMode.NoResize;
            settings.Width = 345.82;
            settings.Height = 255.828;
            settings.Title = "Edit Item";

            IWindowManager manager = new WindowManager();
            AddItemViewModel edit = new AddItemViewModel(LastSelected, Action.Edit);
            bool? result = manager.ShowDialog(edit, null, settings);
            ResultItem = edit.ResultItem;
            if (result == true)
            {
                LastSelected.Name = ResultItem.Name;
                LastSelected.Occurrence = ResultItem.Occurrence;
                LastSelected.Path = ResultItem.Path;
                LastSelected.maxAssignments = ResultItem.maxAssignments;
                LastSelected.ItemPrice = ResultItem.ItemPrice;
                LastSelected.Property = ResultItem.Property;
                LastSelected.Property2 = ResultItem.Property2;
                xmlProvider.EditItem(LastSelected);
                Item temp = LastSelected;
                LastSelected = null;
                LastSelected = temp;
            }
            else
            {

            }
        }

        public bool CanItemRemove
        {
            get
            {
                if (LastSelected != null)
                    return true;
                else
                    return false;
            }
        }
        public void ItemRemove()
        {
            MessageBoxResult result = MessageBox.Show("Do you want remove this Item?", "Removing", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes)
            {
                if (LastSelected != null)
                {
                    switch (LastSelected.Type)
                    {
                        case ItemType.Weapon:
                            xmlProvider.RemoveItem(LastSelected);
                            Weapons.Remove((Weapon)LastSelected);
                            WeaponIndex = -1;
                            break;
                        case ItemType.Armour:
                            xmlProvider.RemoveItem(LastSelected);
                            Armours.Remove((Armour)LastSelected);
                            ArmourIndex = -1;
                            break;
                        case ItemType.Helm:
                            xmlProvider.RemoveItem(LastSelected);
                            Helms.Remove((Helm)LastSelected);
                            HelmIndex = -1;
                            break;
                        case ItemType.Gloves:
                            xmlProvider.RemoveItem(LastSelected);
                            Gloves.Remove((Gloves)LastSelected);
                            GlovesIndex = -1;
                            break;
                        case ItemType.Belt:
                            xmlProvider.RemoveItem(LastSelected);
                            Belts.Remove((Belt)LastSelected);
                            BeltIndex = -1;
                            break;
                        case ItemType.Ring:
                            xmlProvider.RemoveItem(LastSelected);
                            Rings.Remove((Ring)LastSelected);
                            RingIndex = -1;
                            break;
                        case ItemType.Amulet:
                            xmlProvider.RemoveItem(LastSelected);
                            Amulets.Remove((Amulet)LastSelected);
                            AmuletIndex = -1;
                            break;
                        case ItemType.Mixture:
                            xmlProvider.RemoveItem(LastSelected);
                            Mixtures.Remove((Mixture)LastSelected);
                            MixtureIndex = -1;
                            break;
                        case ItemType.Other:
                            xmlProvider.RemoveItem(LastSelected);
                            Others.Remove((Other)LastSelected);
                            OtherIndex = -1;
                            break;
                    }
                }
            }
        }

        public void SortPrice()
        {
            Weapons = new BindableCollection<Weapon>(PriceSort<Weapon>(new List<Weapon>(Weapons)));
            Armours = new BindableCollection<Armour>(PriceSort<Armour>(new List<Armour>(Armours)));
            Helms = new BindableCollection<Helm>(PriceSort<Helm>(new List<Helm>(Helms)));
            Gloves = new BindableCollection<Gloves>(PriceSort<Gloves>(new List<Gloves>(Gloves)));
            Belts = new BindableCollection<Belt>(PriceSort<Belt>(new List<Belt>(Belts)));
            Rings = new BindableCollection<Ring>(PriceSort<Ring>(new List<Ring>(Rings)));
            Amulets = new BindableCollection<Amulet>(PriceSort<Amulet>(new List<Amulet>(Amulets)));
            Mixtures = new BindableCollection<Mixture>(PriceSort<Mixture>(new List<Mixture>(Mixtures)));
            Others = new BindableCollection<Other>(PriceSort<Other>(new List<Other>(Others)));
        }
        public void SortRandom()
        {
            Weapons = new BindableCollection<Weapon>(RandomSort<Weapon>(new List<Weapon>(Weapons)));
            Armours = new BindableCollection<Armour>(RandomSort<Armour>(new List<Armour>(Armours)));
            Helms = new BindableCollection<Helm>(RandomSort<Helm>(new List<Helm>(Helms)));
            Gloves = new BindableCollection<Gloves>(RandomSort<Gloves>(new List<Gloves>(Gloves)));
            Belts = new BindableCollection<Belt>(RandomSort<Belt>(new List<Belt>(Belts)));
            Rings = new BindableCollection<Ring>(RandomSort<Ring>(new List<Ring>(Rings)));
            Amulets = new BindableCollection<Amulet>(RandomSort<Amulet>(new List<Amulet>(Amulets)));
            Mixtures = new BindableCollection<Mixture>(RandomSort<Mixture>(new List<Mixture>(Mixtures)));
            Others = new BindableCollection<Other>(RandomSort<Other>(new List<Other>(Others)));
        }
        public void SortProperties()
        {
            Weapons = new BindableCollection<Weapon>(PropertySort<Weapon>(new List<Weapon>(Weapons)));
            Armours = new BindableCollection<Armour>(PropertySort<Armour>(new List<Armour>(Armours)));
            Helms = new BindableCollection<Helm>(PropertySort<Helm>(new List<Helm>(Helms)));
            Gloves = new BindableCollection<Gloves>(PropertySort<Gloves>(new List<Gloves>(Gloves)));
            Belts = new BindableCollection<Belt>(PropertySort<Belt>(new List<Belt>(Belts)));
            Rings = new BindableCollection<Ring>(PropertySort<Ring>(new List<Ring>(Rings)));
            Amulets = new BindableCollection<Amulet>(PropertySort<Amulet>(new List<Amulet>(Amulets)));
            Mixtures = new BindableCollection<Mixture>(PropertySort<Mixture>(new List<Mixture>(Mixtures)));
            Others = new BindableCollection<Other>(PropertySort<Other>(new List<Other>(Others)));
        }

        private List<T> PriceSort<T>(List<T> list)
        {
            list.Sort((item1, item2) => PriceCompare(item1, item2));
            return list;
        }
        private List<T> PropertySort<T>(List<T> list)
        {
            list.Sort((item1, item2) => PropertyCompare(item1, item2));
            return list;
        }
        private List<T> RandomSort<T>(List<T> list)
        {
            int n = list.Count;
            Random rnd = new Random();
            while (n > 1)
            {
                int k = (rnd.Next(0, n) % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }

        private int PriceCompare<T>(T item1, T item2)
        {
            Item item3 = (Item)(object)item1;
            Item item4 = (Item)(object)item2;
            if (int.Parse(item3.AllPrice) > int.Parse(item4.AllPrice))
                return 1;
            else if (int.Parse(item3.AllPrice) < int.Parse(item4.AllPrice))
                return -1;
            else
                return 0;
        }
        private int PropertyCompare<T>(T item1, T item2)
        {
            Item item3 = (Item)(object)item1;
            Item item4 = (Item)(object)item2;
            if (item3.Properties.Count > item4.Properties.Count)
                return 1;
            else if (item3.Properties.Count < item4.Properties.Count)
                return -1;
            else
                return 0;
        }
    }

    public enum ItemType
    {
        Weapon,
        Armour,
        Helm,
        Gloves,
        Belt,
        Ring,
        Amulet,
        Mixture,
        Other
    }

    public enum PropertyUnit
    {
        percentage,
        number
    }
}
