﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using XML.DataModels;

namespace XML
{
    public class XMLRPG
    {
        XDocument doc;
        XDocument properties;
        string _path = "";

        public XMLRPG()
        {
        }

        public bool Load(string path)
        {
            _path = path;
            try
            {
                doc = XDocument.Load(_path);
                properties = XDocument.Load("properties.xml");
            }
            catch(Exception e)
            {
                return false;
            }
            bool validate = Validate();
            if(!validate)
            {
                doc = null;
            }
            return validate;
        }

        private bool Validate()
        {
            bool errors = false;
            if (doc != null)
            {
                XmlSchemaSet schemas = new XmlSchemaSet();
                schemas.Add("", "schema.xsd");
                doc.Validate(schemas, (o, e) =>
                {
                    errors = true;
                });
                XmlSchemaSet propschema = new XmlSchemaSet();
                propschema.Add("", "properties.xsd");
                properties.Validate(propschema, (o, e) =>
                {
                    errors = true;
                });
                List<XElement> propertyList = properties.Root.Elements("property").ToList();
                List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
                foreach(XElement assignment in assignments)
                {
                    foreach(XElement propertyId in assignment.Elements("propertyId"))
                    {
                        bool was = false;
                        foreach(XElement property in propertyList)
                        {
                            if(property.Element("id").Value == propertyId.Value)
                            {
                                was = true;
                                break;
                            }
                        }
                        if(was != true)
                        {
                            return false;
                        }
                    }
                }

            }
            return (errors) ? false : true;
        }

        public void AddProperty(Property property, Item item)
        {
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();

            if (property.Id == null || property.Id == "")
            {
                string id = propertyList.Last().Element("id").Value;
                id = id.Substring(4, 4);
                int idnumber = int.Parse(id);
                idnumber++;
                string newId = idnumber.ToString();
                while(newId.Length != 4)
                {
                    newId = newId.Insert(0, "0");
                }
                newId = newId.Insert(0,"prop");
                property.Id = newId;
            }

            bool was = false;
            foreach(XElement element in propertyList)
            {
                if (element.Element("id").Value == property.Id)
                {
                    was = true;
                    break;
                }
            }
            if(was == false)
            {
                XElement id = new XElement("id", property.Id);
                XElement name = new XElement("name", property.Name);
                XElement price = new XElement("price", property.Price);
                XElement description = new XElement("description", property.Description);
                XElement value = new XElement("value", new XAttribute("propertyUnit", property.Unit), property.Value);
                XElement newNode = new XElement("property", id, name, price, description, value);
                properties.Root.Add(newNode);
                properties.Save("properties.xml");
            }

            XElement assignmentItem = null;
            foreach (XElement assignment in assignments)
            {
                if (assignment.Element("itemId").Value == item.Id)
                    assignmentItem = assignment;
            }
            if(assignmentItem == null)
            {
                XElement newAssignment = new XElement("assignment",new XElement("itemId", item.Id));
                doc.Root.Element("assignedProperty").Add(newAssignment);
                doc.Save(_path);
                assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
                foreach (XElement assignment in assignments)
                {
                    if (assignment.Element("itemId").Value == item.Id)
                    {
                        assignmentItem = assignment;
                        break;
                    }
                }
            }
            assignmentItem.Add(new XElement("propertyId", property.Id));
            doc.Save(_path);
        }

        public void RemoveProperty(Property property, Item item)
        {
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            XElement assignment = new XElement("ss");
            foreach(XElement elem in assignments)
            {
                if (elem.Element("itemId").Value == item.Id)
                    assignment = elem;
            }
            foreach(XElement prop in assignment.Elements("propertyId"))
            {
                if (prop.Value == property.Id)
                    prop.Remove();
            }
            if(assignment.Elements("propertyId").ToList().Count == 0)
            {
                assignment.Remove();
            }
            doc.Save(_path);
        }

        public void EditProperty(Property property)
        {
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            XElement prop = null;
            foreach (XElement element in propertyList)
            {
                if (element.Element("id").Value == property.Id)
                {
                    prop = element;
                    break;
                }
            }
            if (prop != null)
            {
                prop.Element("name").SetValue(property.Name);
                prop.Element("price").SetValue(property.Price);
                prop.Element("description").SetValue(property.Description);
                prop.Element("value").SetValue(property.Value);
                prop.Element("value").Attribute("propertyUnit").SetValue(property.Unit);
                properties.Save("properties.xml");
            }
        }

        public void AddItem(Item item)
        {
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> typeitemList = new List<XElement>();
            string type = "";
            switch(item.Type)
            {
                case ItemType.Weapon:
                    type = "wep";
                    break;
                case ItemType.Armour:
                    type = "arm";
                    break;
                case ItemType.Helm:
                    type = "hel";
                    break;
                case ItemType.Gloves:
                    type = "glo";
                    break;
                case ItemType.Belt:
                    type = "bel";
                    break;
                case ItemType.Ring:
                    type = "rin";
                    break;
                case ItemType.Amulet:
                    type = "amu";
                    break;
                case ItemType.Mixture:
                    type = "mix";
                    break;
                case ItemType.Other:
                    type = "oth";
                    break;
            }
            foreach (XElement element in itemList)
                if (element.Element("id").Value.Substring(0, 3) == type)
                    typeitemList.Add(element);

            if (item.Id == null || item.Id == "")
            {
                string id = "";
                if (typeitemList.Count > 0)
                {
                    id = typeitemList.Last().Element("id").Value;
                    id = id.Substring(3, 4);
                    int idnumber = int.Parse(id);
                    idnumber++;
                    string newId = idnumber.ToString();
                    while (newId.Length != 4)
                    {
                        newId = newId.Insert(0, "0");
                    }
                    newId = newId.Insert(0, type);
                    item.Id = newId;
                }
                else
                {
                    item.Id = type + "0001";
                }   
            }

            string from = item.Path;
            string to = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) + @"\images\" +item.Id+ Path.GetExtension(item.Path);
            to = to.Substring(6, to.Length - 6);
            File.Copy(from, to, true);
            item.Path = @"./images/" + item.Id + Path.GetExtension(item.Path);

            XElement newNode = null;
            if(item.Type == ItemType.Weapon)
            {
                XElement id = new XElement("id", item.Id);
                XElement name = new XElement("name", item.Name);
                XElement occurrence = new XElement("occurrence", item.Occurrence);
                XElement itype = new XElement("type", "weapon");
                string dates = "";
                dates += DateTime.Now.Year.ToString() + "-";
                if(DateTime.Now.Month.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Month.ToString() + "-";
                if (DateTime.Now.Day.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Day.ToString();
                XElement date = new XElement("addDate", dates);
                XElement damage = new XElement("damage", item.Property+" "+item.Property2);
                XElement price = new XElement("price", item.ItemPrice);
                XElement assignments = new XElement("maxAssignments", item.maxAssignments);
                XElement path = new XElement("imagePath", item.Path);
                newNode = new XElement("item", id, name, occurrence, itype, date, damage, price,assignments,path);
            }
            else if(item.Type == ItemType.Armour || item.Type == ItemType.Helm || item.Type == ItemType.Gloves || item.Type == ItemType.Belt)
            {
                XElement id = new XElement("id", item.Id);
                XElement name = new XElement("name", item.Name);
                XElement occurrence = new XElement("occurrence", item.Occurrence);
                string istype = null;
                switch(item.Type)
                {
                    case ItemType.Armour:
                        istype = "armour";
                        break;
                    case ItemType.Helm:
                        istype = "helm";
                        break;
                    case ItemType.Gloves:
                        istype = "gloves";
                        break;
                    case ItemType.Belt:
                        istype = "belt";
                        break;
                }
                XElement itype = new XElement("type", istype);
                string dates = "";
                dates += DateTime.Now.Year.ToString() + "-";
                if (DateTime.Now.Month.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Month.ToString() + "-";
                if (DateTime.Now.Day.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Day.ToString();
                XElement date = new XElement("addDate", dates);
                XElement armour = new XElement("armour", item.Property);
                XElement price = new XElement("price", item.ItemPrice);
                XElement assignments = new XElement("maxAssignments", item.maxAssignments);
                XElement path = new XElement("imagePath", item.Path);
                newNode = new XElement("item", id, name, occurrence, itype, date, armour, price, assignments, path);
            }
            else
            {
                XElement id = new XElement("id", item.Id);
                XElement name = new XElement("name", item.Name);
                XElement occurrence = new XElement("occurrence", item.Occurrence);
                XElement itype = new XElement("type", "weapon");
                string dates = "";
                dates += DateTime.Now.Year.ToString() + "-";
                if (DateTime.Now.Month.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Month.ToString() + "-";
                if (DateTime.Now.Day.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Day.ToString();
                XElement date = new XElement("addDate", dates);
                XElement armour = new XElement("armour", "n/d");
                XElement price = new XElement("price", item.ItemPrice);
                XElement assignments = new XElement("maxAssignments", item.maxAssignments);
                XElement path = new XElement("imagePath", item.Path);
                newNode = new XElement("item", id, name, occurrence, itype, date, armour, price, assignments, path);
            }
            doc.Root.Element("itemList").Add(newNode);
            doc.Save(_path);
        }

        public void RemoveItem(Item item)
        {
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            foreach (XElement assignment in assignments)
            {
                if (assignment.Element("itemId").Value == item.Id)
                    assignment.Remove();
            }
            foreach(XElement itemel in itemList)
            {
                if (itemel.Element("id").Value == item.Id)
                    itemel.Remove();
            }
            string delpath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) + @"\images\" + item.Id + Path.GetExtension(item.Path);
            delpath = delpath.Substring(6, delpath.Length - 6);
            File.Delete(delpath);
            doc.Save(_path);
        }

        public void EditItem(Item item)
        {
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            XElement itemel = null;
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value == item.Id)
                {
                    itemel = element;
                    break;
                }
            }
            if (itemel != null)
            {
                itemel.Element("name").SetValue(item.Name);
                itemel.Element("occurrence").SetValue(item.Occurrence);
                string dates = "";
                dates += DateTime.Now.Year.ToString() + "-";
                if (DateTime.Now.Month.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Month.ToString() + "-";
                if (DateTime.Now.Day.ToString().Length == 1)
                    dates += "0";
                dates += DateTime.Now.Day.ToString();
                itemel.Element("addDate").SetValue(dates);
                switch(item.Type)
                {
                    case ItemType.Weapon:
                        itemel.Element("damage").SetValue(item.Property + " " + item.Property2);
                        break;
                    case ItemType.Armour:
                    case ItemType.Belt:
                    case ItemType.Gloves:
                    case ItemType.Helm:
                        itemel.Element("armour").SetValue(item.Property);
                        break;
                }
                itemel.Element("price").SetValue(item.ItemPrice);
                itemel.Element("maxAssignments").SetValue(item.maxAssignments);
                if (itemel.Element("imagePath").Value != item.Path)
                {
                    string from = item.Path;
                    string to = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase) + @"\images\" + item.Id + Path.GetExtension(item.Path);
                    to = to.Substring(6, to.Length - 6);
                    File.Copy(from, to, true);
                    item.Path = @"./images/" + item.Id + Path.GetExtension(item.Path);
                }
                doc.Save(_path);
            }
        }

        public List<Property> getProperties()
        {
            List<Property> props = new List<Property>();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in propertyList)
            {
                Property temp = new Property();
                temp.Id = element.Element("id").Value;
                temp.Name = element.Element("name").Value;
                temp.Price = element.Element("price").Value;
                temp.Description = element.Element("description").Value;
                if (element.Element("value").Attribute("propertyUnit").Value == "percentage")
                    temp.Unit = PropertyUnit.percentage;
                else
                    temp.Unit = PropertyUnit.number;
                temp.Value = element.Element("value").Value;
                props.Add(temp);
            }
            return props;
        }

        public List<Weapon> getWeapons()
        {
            List<Weapon> weapons = new List<Weapon>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "wep")
                {
                    Weapon temp = new Weapon();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    string[] damage = element.Element("damage").Value.Split(' ');
                    temp.MinDamage = damage[0];
                    temp.MaxDamage = damage[1];
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach(XElement assignment in assignments)
                    {
                        if(assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach(XElement prop in propertyList)
                            {
                                if(prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    weapons.Add(temp);
                }
            }
            return weapons;
        }

        public List<Armour> getArmours()
        {
            List<Armour> armours = new List<Armour>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "arm")
                {
                    Armour temp = new Armour();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.Defense = element.Element("armour").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    armours.Add(temp);
                }
            }
            return armours;
        }

        public List<Helm> getHelms()
        {
            List<Helm> helms = new List<Helm>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "hel")
                {
                    Helm temp = new Helm();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.Defense = element.Element("armour").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    helms.Add(temp);
                }
            }
            return helms;
        }

        public List<Gloves> getGloves()
        {
            List<Gloves> gloves = new List<Gloves>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "glo")
                {
                    Gloves temp = new Gloves();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.Defense = element.Element("armour").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    gloves.Add(temp);
                }
            }
            return gloves;
        }

        public List<Belt> getBelts()
        {
            List<Belt> belts = new List<Belt>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "bel")
                {
                    Belt temp = new Belt();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.Defense = element.Element("armour").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    belts.Add(temp);
                }
            }
            return belts;
        }

        public List<Ring> getRings()
        {
            List<Ring> rings = new List<Ring>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "rin")
                {
                    Ring temp = new Ring();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    rings.Add(temp);
                }
            }
            return rings;
        }

        public List<Amulet> getAmulets()
        {
            List<Amulet> amulets = new List<Amulet>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "amu")
                {
                    Amulet temp = new Amulet();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    amulets.Add(temp);
                }
            }
            return amulets;
        }

        public List<Mixture> getMixtures()
        {
            List<Mixture> mixtures = new List<Mixture>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "mix")
                {
                    Mixture temp = new Mixture();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    mixtures.Add(temp);
                }
            }
            return mixtures;
        }

        public List<Other> getOthers()
        {
            List<Other> others = new List<Other>();
            List<XElement> itemList = doc.Root.Element("itemList").Elements("item").ToList();
            List<XElement> assignments = doc.Root.Element("assignedProperty").Elements("assignment").ToList();
            List<XElement> propertyList = properties.Root.Elements("property").ToList();
            foreach (XElement element in itemList)
            {
                if (element.Element("id").Value.Substring(0, 3) == "oth")
                {
                    Other temp = new Other();
                    temp.Id = element.Element("id").Value;
                    temp.Name = element.Element("name").Value;
                    temp.ItemPrice = element.Element("price").Value;
                    temp.maxAssignments = element.Element("maxAssignments").Value;
                    temp.Occurrence = element.Element("occurrence").Value;
                    temp.Path = element.Element("imagePath").Value;
                    XElement assign = null;
                    foreach (XElement assignment in assignments)
                    {
                        if (assignment.Element("itemId").Value == temp.Id)
                        {
                            assign = assignment;
                            break;
                        }
                    }
                    if (assign != null)
                    {
                        foreach (XElement propId in assign.Elements("propertyId"))
                        {
                            foreach (XElement prop in propertyList)
                            {
                                if (prop.Element("id").Value == propId.Value)
                                {
                                    Property props = new Property();
                                    props.Id = prop.Element("id").Value;
                                    props.Name = prop.Element("name").Value;
                                    props.Price = prop.Element("price").Value;
                                    props.Description = prop.Element("description").Value;
                                    if (prop.Element("value").Attribute("propertyUnit").Value == "percentage")
                                        props.Unit = PropertyUnit.percentage;
                                    else
                                        props.Unit = PropertyUnit.number;
                                    props.Value = prop.Element("value").Value;
                                    temp.Properties.Add(props);
                                }
                            }
                        }
                    }
                    others.Add(temp);
                }
            }
            return others;
        }
    }
}
