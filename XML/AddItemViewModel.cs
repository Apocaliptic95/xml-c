﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using XML.DataModels;

namespace XML
{
    public class AddItemViewModel : Screen
    {
        Item _lastSelected;
        public Item ResultItem;

        public AddItemViewModel(Item LastSelected, Action action)
        {
            _lastSelected = LastSelected;
            Types = new BindableCollection<ItemType>();
            Types.Add(ItemType.Weapon);
            Types.Add(ItemType.Armour);
            Types.Add(ItemType.Helm);
            Types.Add(ItemType.Gloves);
            Types.Add(ItemType.Belt);
            Types.Add(ItemType.Ring);
            Types.Add(ItemType.Amulet);
            Types.Add(ItemType.Mixture);
            Types.Add(ItemType.Other);
            Visibility = "Visible";
            Visibility2 = "Visible";
            if (action == Action.Add)
            {
                TypeVisibility = "Visible";
                PropertyName = "Min Damage:";
                Margin = "9,0,0,0";
                Name = "";
            }
            else
            {
                int property, property2,price,assignments,occurrence;
                TypeVisibility = "Collapsed";
                Name = LastSelected.Name;
                if (int.TryParse(LastSelected.Occurrence, out occurrence))
                    Occurrence = occurrence;
                else
                    Occurrence = 0;
                if (int.TryParse(LastSelected.Property, out property))
                    Property = property;
                else
                    Property = 0;
                if (int.TryParse(LastSelected.Property2, out property2))
                    Property2 = property2;
                else
                    Property2 = 0;
                if (int.TryParse(LastSelected.ItemPrice, out price))
                    Price = price;
                else
                    Price = 0;
                if (int.TryParse(LastSelected.maxAssignments, out assignments))
                    MaxAssignments = assignments;
                else
                    MaxAssignments = 0;
                SelectedType = LastSelected.Type;
                Path = LastSelected.Path;
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void Apply()
        {
            switch(SelectedType)
            {
                case ItemType.Weapon:
                    ResultItem = new Weapon();
                    ResultItem.Name = Name;
                    ResultItem.Property = Property.ToString();
                    ResultItem.Property2 = Property2.ToString();
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Armour:
                    ResultItem = new Armour();
                    ResultItem.Name = Name;
                    ResultItem.Property = Property.ToString();
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Helm:
                    ResultItem = new Helm();
                    ResultItem.Name = Name;
                    ResultItem.Property = Property.ToString();
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Gloves:
                    ResultItem = new Gloves();
                    ResultItem.Name = Name;
                    ResultItem.Property = Property.ToString();
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Belt:
                    ResultItem = new Belt();
                    ResultItem.Name = Name;
                    ResultItem.Property = Property.ToString();
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Ring:
                    ResultItem = new Ring();
                    ResultItem.Name = Name;
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Amulet:
                    ResultItem = new Amulet();
                    ResultItem.Name = Name;
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Mixture:
                    ResultItem = new Mixture();
                    ResultItem.Name = Name;
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
                case ItemType.Other:
                    ResultItem = new Other();
                    ResultItem.Name = Name;
                    ResultItem.Occurrence = Occurrence.ToString();
                    ResultItem.ItemPrice = Price.ToString();
                    ResultItem.Path = Path;
                    ResultItem.maxAssignments = MaxAssignments.ToString();
                    break;
            }
            TryClose(true);
        }

        public bool CanApply
        {
            get
            {
                bool damage = false;
                bool armour = false;
                bool name = false;
                bool occurrence = false;
                bool path = false;
                bool assignments = false;
                bool price = false;
                if (SelectedType == ItemType.Weapon)
                {
                    if (Property >= 0 && Property <= 5000 && Property2 >= 0 && Property2 <= 5000 && Property <= Property2)
                        damage = true;
                    else
                        damage = false;
                }
                else
                    damage = true;
                if (SelectedType == ItemType.Armour)
                {
                    if (Property >= 0 && Property <= 5000)
                        armour = true;
                    else
                        armour = false;
                }
                else
                    armour = true;
                if (Name != null && Name != "" && Regex.IsMatch(Name, @"([A-Z][a-z]+[ ]?)+"))
                    name = true;
                if (Occurrence >= 0 && Occurrence <= 10)
                    occurrence = true;
                if (Path != null && Path != "")
                    path = true;
                if (MaxAssignments >= 0 && MaxAssignments <= 50)
                    assignments = true;
                if (Price >= 0 && Price <= 32767)
                    price = true;
                return damage && armour && name && occurrence && path && assignments && price;
            }
        }

        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
            set
            {
                _propertyName = value;
                NotifyOfPropertyChange("PropertyName");
            }
        }

        private string _visibility;
        private string _visibility2;
        public string Visibility
        {
            get
            {
                return _visibility;
            }
            set
            {
                _visibility = value;
                NotifyOfPropertyChange("Visibility");
            }
        }
        public string Visibility2
        {
            get
            {
                return _visibility2;
            }
            set
            {
                _visibility2 = value;
                NotifyOfPropertyChange("Visibility2");
            }
        }
        private string _typeVisibility;
        public string TypeVisibility
        {
            get
            {
                return _typeVisibility;
            }
            set
            {
                _typeVisibility = value;
                NotifyOfPropertyChange("TypeVisibility");
            }
        }
        private string _margin;
        public string Margin
        {
            get
            {
                return _margin;
            }
            set
            {
                _margin = value;
                NotifyOfPropertyChange("Margin");
            }
        }

        private ImageSource _image;
        public ImageSource Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
                NotifyOfPropertyChange("Image");
            }
        }

        public void Choose()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".png";
            dlg.Filter = "PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                BitmapImage image = new BitmapImage(new Uri(filename));
                Image = image;
                Path = filename;
            }
        }

        private BindableCollection<ItemType> _types;
        public BindableCollection<ItemType> Types
        {
            get
            {
                return _types;
            }
            set
            {
                _types = value;
                NotifyOfPropertyChange("Types");
            }
        }
        private ItemType _selectedType;
        public ItemType SelectedType
        {
            get
            {
                return _selectedType;
            }
            set
            {
                _selectedType = value;
                NotifyOfPropertyChange("SelectedType");
                if(SelectedType == ItemType.Weapon)
                {
                    PropertyName = "Min Damage:";
                    Visibility = "Visible";
                    Visibility2 = "Visible";
                    Margin = "9,0,0,0";
                }
                else if(SelectedType == ItemType.Armour || SelectedType == ItemType.Helm || SelectedType == ItemType.Gloves || SelectedType == ItemType.Belt)
                {
                    PropertyName = "Armour:";
                    Margin = "38,0,0,0";
                    Visibility = "Visible";
                    Visibility2 = "Collapsed";
                }
                else
                {
                    Visibility = "Collapsed";
                    Visibility2 = "Collapsed";
                }
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyOfPropertyChange("Name");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _occurrence;
        public int Occurrence
        {
            get
            {
                return _occurrence;
            }
            set
            {
                _occurrence = value;
                NotifyOfPropertyChange("Occurrence");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _property;
        public int Property
        {
            get
            {
                return _property;
            }
            set
            {
                _property = value;
                NotifyOfPropertyChange("Property");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _property2;
        public int Property2
        {
            get
            {
                return _property2;
            }
            set
            {
                _property2 = value;
                NotifyOfPropertyChange("Property2");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _maxAssignments;
        public int MaxAssignments
        {
            get
            {
                return _maxAssignments;
            }
            set
            {
                _maxAssignments = value;
                NotifyOfPropertyChange("MaxAssignments");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _price;
        public int Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                NotifyOfPropertyChange("Price");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private string _path;
        public string Path
        {
            get
            {
                return _path;
            }
            set
            {
                _path = value;
                NotifyOfPropertyChange("Path");
                NotifyOfPropertyChange("CanApply");
            }
        }
    }

    public enum Action
    {
        Edit,
        Add
    }

    public enum Result
    {
        cancel,
        apply
    }
}
