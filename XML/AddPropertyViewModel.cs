﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XML.DataModels;

namespace XML
{
    public class AddPropertyViewModel : Screen
    {
        Property SelectedProperty;
        public Property ResultProperty;

        public AddPropertyViewModel(Property SelectedProperty, Action action)
        {
            this.SelectedProperty = SelectedProperty;
            Units = new BindableCollection<PropertyUnit>();
            Units.Add(PropertyUnit.percentage);
            Units.Add(PropertyUnit.number);
            if (action == Action.Edit)
            {
                int price, value;
                Name = SelectedProperty.Name;
                if (int.TryParse(SelectedProperty.Value, out value))
                    Value = value;
                else
                    Value = 0;
                if (int.TryParse(SelectedProperty.Price, out price))
                    Price = price;
                else
                    Price = 0;
                SelectedUnit = SelectedProperty.Unit;
                Description = SelectedProperty.Description;
            }
        }

        public void Cancel()
        {
            TryClose(false);
        }

        public void Apply()
        {
            ResultProperty = new Property();
            ResultProperty.Name = Name;
            ResultProperty.Unit = SelectedUnit;
            ResultProperty.Value = Value.ToString();
            ResultProperty.Description = Description;
            ResultProperty.Price = Price.ToString();
            TryClose(true);
        }

        public bool CanApply
        {
            get
            {
                bool name = false;
                bool description = false;
                bool price = false;
                bool value = false;
                if (Name != null && Name != "" && Regex.IsMatch(Name, @"([A-Z][a-z]+[ ]?)+"))
                    name = true;
                if (Description != null && Description.Length > 5)
                    description = true;
                if (Price >= 0 && Price <= 32767)
                    price = true;
                if (SelectedUnit == PropertyUnit.percentage)
                {
                    if (Value >= 0 && Value <= 100)
                        value = true;
                }
                else if (SelectedUnit == PropertyUnit.number)
                {
                    if (Value >= 0 && Value <= 32767)
                        value = true;
                }
                return  name && description && price && value;
            }
        }

        private string _propertyName;
        public string PropertyName
        {
            get
            {
                return _propertyName;
            }
            set
            {
                _propertyName = value;
                NotifyOfPropertyChange("PropertyName");
            }
        }

        private BindableCollection<PropertyUnit> _units;
        public BindableCollection<PropertyUnit> Units
        {
            get
            {
                return _units;
            }
            set
            {
                _units = value;
                NotifyOfPropertyChange("Units");
            }
        }
        private PropertyUnit _selectedUnit;
        public PropertyUnit SelectedUnit
        {
            get
            {
                return _selectedUnit;
            }
            set
            {
                _selectedUnit = value;
                NotifyOfPropertyChange("SelectedUnit");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyOfPropertyChange("Name");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _price;
        public int Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
                NotifyOfPropertyChange("Price");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private int _value;
        public int Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                NotifyOfPropertyChange("Value");
                NotifyOfPropertyChange("CanApply");
            }
        }

        private string _description;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
                NotifyOfPropertyChange("Description");
                NotifyOfPropertyChange("CanApply");
            }
        }
    }
}
