﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XML.DataModels;

namespace XML
{
    public class PropertyListViewModel : Screen
    {

        public PropertyListViewModel(List<Property> propertyList)
        {
            Propertys = new BindableCollection<Property>(propertyList);
        }

        private BindableCollection<Property> _propertys;
        public BindableCollection<Property> Propertys
        {
            get
            {
                return _propertys;
            }
            set
            {
                _propertys = value;
                NotifyOfPropertyChange("Propertys");
            }
        }
        private Property _selectedProperty;
        public Property SelectedProperty
        {
            get
            {
                return _selectedProperty;
            }
            set
            {
                _selectedProperty = value;
                NotifyOfPropertyChange("SelectedProperty");
                NotifyOfPropertyChange("CanApply");
            }
        }
        public Property ResultProperty;

        public void Cancel()
        {
            TryClose(false);
        }

        public void Apply()
        {
            ResultProperty = SelectedProperty;
            TryClose(true);
        }

        public bool CanApply
        {
            get
            {
                if (SelectedProperty != null)
                    return true;
                else
                    return false;
            }
        }
    }
}
